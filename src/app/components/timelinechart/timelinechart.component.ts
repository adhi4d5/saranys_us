import { Component, OnInit ,Input} from '@angular/core';

@Component({
  selector: 'app-timelinechart',
  templateUrl: './timelinechart.component.html',
  styleUrls: ['./timelinechart.component.css']
})
export class TimelinechartComponent implements OnInit {
  @Input() timeLineData:any;
  showSubTimeLine:boolean = false;
  SubTimeLineData:any = [];
  parentIndex: number = null;
  childIndex: number = null;
  constructor() { }

  ngOnInit(): void {
    // console.log("timeLineData",this.timeLineData);
  }

  selectedval(device,i,j){
    this.parentIndex = i;
    this.childIndex = j;
    this.showSubTimeLine = false;
    this.SubTimeLineData = [];
    this.SubTimeLineData = this.timeLineData.data[i].data[j];  
    this.showSubTimeLine = true;
  }

}
